(function() {

    var io = window.io.connect(window.location.href);
    var $feeds = {};
    var initialized;
    var initialized_timeout;

    var templates = {};

    templates.title = ""+
    "<div class='title'>"+
    "<a href='{url}' target='_blank'><div>{title}</div></a>"+
    "</div>";

    templates.item = ""+
    "<div class='date'>{date}</div>"+
    "<img class='{img_class}' src='{image_url}'/>"+
    "<a href='{link}' target='_blank'><h2>{title}</h2></a>"+
    "<div class='p {p_class}'>{summary}</div>"+
    "";

    io.on("feed", function(obj) {
        var feed = $feeds[obj.path];
        if (!feed) {
            feed = $feeds[obj.path] = document.createElement("div");
            feed.className = "feed";
            feed.innerHTML = prepare(templates.title, {
                url: obj.path,
                title: obj.path.split("/")[2]
            });
            document.body.appendChild(feed);
            var feeds = document.getElementsByClassName("feed");
            var new_width = 90 / feeds.length;
            for (var i = 0; i < feeds.length; i++) {
                feeds[i].style.width = new_width + "%";
            }
            io.on(obj.path, putItem.bind(null, feed));
        }
        obj.items.forEach(putItem.bind(null, feed));
        if (initialized_timeout) clearTimeout(initialized_timeout);
        initialized_timeout = setTimeout(function() {
            clearTimeout(initialized_timeout);
            initialized = true;
        }, 20 * 1000);
    });

    function putItem(feed, item) {
        var $item = document.createElement("div");
        $item.className = "item";
        if (initialized) {
            $item.className += " highlight";
            setTimeout(function() {
                $item.style.background = "white";
            }, 1000);
        }
        item.image_url = item.image.url || "";
        item.img_class = item.image_url ? "" : "hidden";
        item.date = new Date(item.date).toLocaleString().replace(/ [A-Z]{3}.*/, "");
        item.summary = sanitize(item.summary);
        item.p_class = item.summary ? "" : "hidden";
        $item.innerHTML = prepare(templates.item, item);
        feed.insertBefore($item, feed.firstChild.nextSibling);
        if (feed.childNodes.length > 10) {
            feed.removeChild(feed.lastChild);
        }
    }

    io.on("disconnect", function() {
        io.socket.reconnect();
    });

    function prepare(template, item) {
        console.log(item);
        var t = ""+template;
        var _k, k;
        for (k in item) {
            _k = "{"+k+"}";
            if (template.indexOf(_k) > -1) {
                t = t.replace(_k, item[k]);
            }
        }
        return t;
    }

    function sanitize(text) {
        return text
            .replace(/<\/{0,1}div[^>]*>/g, "")
            .replace(/<\/{0,1}h[^>]*>/g, "")  // H1..HN, head
            .replace(/<\/{0,1}f[^>]*>/g, "")  // form
            .replace(/<\/{0,1}i[^>]*>/g, "")  // img, iframe, input
            .replace(/<\/{0,1}l[^>]*>/g, "")  // link
            .replace(/<\/{0,1}p[^>]*>/g, "")  // p
            .replace(/<\/{0,1}s[^>]*>/g, "")  // script, style
            .replace(/<\/{0,1}t[^>]*>/g, ""); // table, tr, td
    }

})();
