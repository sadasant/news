var express = require("express");
var path    = require("path");

module.exports = function(exp) {
    exp.configure(function(){

        function normalize(dir) {
            return express.static(path.normalize(__dirname + dir));
        }

        exp.use("/css", normalize("/../src/css"));
        exp.use("/js", normalize("/../src/js"));

        exp.use("/", function(req, res) {
            res.sendfile(path.normalize(__dirname + "/../index.html"));
        });
    });
};
