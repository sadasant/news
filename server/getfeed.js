var request = require("request");
var fp      = require("feedparser");

module.exports = function(path, callback) {
    var req    = request(path);
    var parser = new fp();
    var items  = [];

    req.on("error", function(error) {
        console.error("Failed the request of "+path, error);
    });

    req.on("response", function(res) {
        if (res.statusCode !== 200) {
            return this.emit("error", new Error("Bad status code: ", res.statusCode));
        }
        this.pipe(parser);
    });

    parser.on("error", function(error) {
        console.error("Failed parsing "+path, error);
    });

    parser.on("readable", function() {
        items.push(this.read());
    });

    parser.on("end", function() {
        if (!callback) return;
        callback(items.sort(function(a, b) {
            return new Date(a.date) - new Date(b.date);
        }));
    });
};
