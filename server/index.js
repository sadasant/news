var port    = process.env.PORT || 8888;
var exp     = require("express")();
var path    = require("path");
var http    = require("http");
var server  = http.createServer(exp);
var io      = require("socket.io").listen(server);
var getFeed = require("./getfeed");
var utils   = require("./utils");

io.set("log level", 1);

require("./config")(exp);

var feeds = {
    "http://news.sky.com/feeds/rss/home.xml": [],
    "http://feeds.bbci.co.uk/news/rss.xml":   [],
    "http://www.reddit.com/new/.rss":         []
};

function readInterval(path, seconds) {
    var host = path.split("/")[2];
    utils.log("Fetching", host);
    var stats = {
        new: 0,
        old: 0
    };
    getFeed(path, function(items) {
        utils.log("Received", items.length, "items from", host);
        items = items.slice(-10);
        items.every(function(item) {
            var is_new = feeds[path].every(function(e) {
                return e.title !== item.title;
            });
            if (is_new) {
                stats.new++;
                feeds[path].push(item);
                io.sockets.emit(path, item);
            } else {
                stats.old++;
            }
            return true;
        });
        utils.log(host, "items status:", stats.new, "new,", stats.old, "old");
        feeds[path] = feeds[path].slice(-10);
        setTimeout(readInterval.bind(null, path, seconds), 1000 * seconds);
    });
}

for (var k in feeds) {
    readInterval(k, 30);
}

io.sockets.on("connection", function (socket) {
    for (var k in feeds) {
        socket.emit("feed", {
            path  : k,
            items : feeds[k].slice(-10)
        });
    }
});

server.listen(port, function(){
    utils.log("Server running at http://127.0.0.1:" + port + "/");
});
