var utils = module.exports;

utils.log = function() {
    var args = Array.prototype.slice.call(arguments);
    var req  = args[0];
    var date = new Date().toISOString();

    args = [date].concat(args);
    console.log.apply(console, args);
};
